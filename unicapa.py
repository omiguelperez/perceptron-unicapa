# -*- coding: utf-8 -*-

import random
import pickle
from pylab import ylim, show, plot, ion


class Perceptron:
    def __init__(self, cantidad_entradas, cantidad_salidas):
        self.m = cantidad_entradas
        self.n = cantidad_salidas
        self.p = None # Cantidad de patrones
        self.errores = []
        self.inicializar_pesos()
        self.inicializar_umbrales()
        self.establecer_parametros_entrenamiento_iniciales()

    def inicializar_pesos(self):
        # w[neurona_salida, neurona_entrada]
        self.w = [[random.uniform(-1.0, 1.0) for i in range(self.n)] for j in range(self.m)]

    def inicializar_umbrales(self):
        # u[neurona_salida]
        self.u = [random.uniform(-1.0, 1.0) for i in range(self.n)]

    def configurar_red_antes_de_entrenamiento(self):
        self.y = [[0.0 for p in range(self.p)] for i in range(self.n)] # y[[salida 1], [salida 2], ...]
        self.el = [[0.0 for i in range(self.n)] for p in range(self.p)] # el[[errores patron 1], [errores patron 2], ...]
        self.ep = [0.0 for p in range(self.p)] # ep[errores...]
        self.rms = 0.0

    def establecer_parametros_entrenamiento_iniciales(self):
        self.trainParam = {}
        self.trainParam['epochs'] = 1000
        self.trainParam['goal'] = 0.00
        self.trainParam['lr'] = 0.01

    def establecer_cantidad_de_patrones(self, cantidad_patrones = None):
        self.p = len(self.x) if cantidad_patrones == None else cantidad_patrones

    def calcular_salida_de_la_red(self, pattern):
        for i in range(self.n):
            resultado = 0
            for j in range(self.m):
                resultado += self.x[pattern][j] * self.w[j][i]
            resultado -= self.u[i]
            self.y[i][pattern] = self.aplicar_funcion_de_activacion(resultado)

    def calcular_errores_lineales_del_patron(self, pattern):
        self.el[pattern] = [(self.yd[i][pattern] - self.y[i][pattern]) for i in range(self.n)]

    def calcular_error_del_patron(self, pattern):
        self.ep[pattern] = sum([abs(err) for err in self.el[pattern]]) / self.n

    def aplicar_funcion_de_activacion(self, value):
        return 1 if value >= 0 else 0

    def actualzar_pesos_y_umbrales(self, pattern):
        for i in range(self.n):
            for j in range(self.m):
                self.w[j][i] += self.trainParam['lr'] * self.el[pattern][i] * self.x[pattern][j]
            self.u[i] += self.trainParam['lr'] * self.el[pattern][i]

    def calcular_error_rms(self):
        self.rms = sum(self.ep) / self.p
        self.errores.append(self.rms)
        print('RMS: %s' % self.rms)

    def graficar_errores(self):
        ylim(-1,1)
        plot(self.errores)
        show()

    def guardar_pesos_y_umbrales(self):
        pickle.dump(self.w, open('pesos.db', 'wb'))
        pickle.dump(self.u, open('umbrales.db', 'wb'))

    def train(self, patrones_de_entrada, salida_deseada):
        self.x = patrones_de_entrada # x[[patron 1], [patron 2], ...]
        self.yd = salida_deseada # y[[salida 1], [salida 2], ...]
        self.establecer_cantidad_de_patrones()
        self.configurar_red_antes_de_entrenamiento()
        for iteration in range(self.trainParam['epochs']):
            for pattern in range(self.p):
                self.calcular_salida_de_la_red(pattern)
                self.calcular_errores_lineales_del_patron(pattern)
                self.calcular_error_del_patron(pattern)
                self.actualzar_pesos_y_umbrales(pattern)
            self.calcular_error_rms()
            if self.rms <= self.trainParam['goal']:
                self.graficar_errores()
                self.guardar_pesos_y_umbrales()
                break

    def sim(self, inputs):
        pass

entradas = [
    [1, 1],
    [1, 0],
    [0, 1],
    [0, 0]
]
salidas = [
    [1, 0, 0, 0]
]

net = Perceptron(len(entradas[0]), len(salidas))
net.trainParam['epochs'] = 1000
net.train(entradas, salidas)
